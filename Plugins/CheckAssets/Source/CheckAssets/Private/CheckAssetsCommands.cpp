// Copyright Epic Games, Inc. All Rights Reserved.

#include "CheckAssetsCommands.h"

#define LOCTEXT_NAMESPACE "FCheckAssetsModule"

void FCheckAssetsCommands::RegisterCommands()
{
	UI_COMMAND(PluginAction, "CheckAssets", "Execute CheckAssets action", EUserInterfaceActionType::Button, FInputChord());
}

#undef LOCTEXT_NAMESPACE
