// Copyright Epic Games, Inc. All Rights Reserved.

#include "CheckAssetsStyle.h"
#include "CheckAssets.h"
#include "Framework/Application/SlateApplication.h"
#include "Styling/SlateStyleRegistry.h"
#include "Slate/SlateGameResources.h"
#include "Interfaces/IPluginManager.h"
#include "Styling/SlateStyleMacros.h"

#define RootToContentDir Style->RootToContentDir

TSharedPtr<FSlateStyleSet> FCheckAssetsStyle::StyleInstance = nullptr;

void FCheckAssetsStyle::Initialize()
{
	if (!StyleInstance.IsValid())
	{
		StyleInstance = Create();
		FSlateStyleRegistry::RegisterSlateStyle(*StyleInstance);
	}
}

void FCheckAssetsStyle::Shutdown()
{
	FSlateStyleRegistry::UnRegisterSlateStyle(*StyleInstance);
	ensure(StyleInstance.IsUnique());
	StyleInstance.Reset();
}

FName FCheckAssetsStyle::GetStyleSetName()
{
	static FName StyleSetName(TEXT("CheckAssetsStyle"));
	return StyleSetName;
}


const FVector2D Icon16x16(16.0f, 16.0f);
const FVector2D Icon20x20(20.0f, 20.0f);
const FVector2D Icon40x40(40.0f, 40.0f);

TSharedRef< FSlateStyleSet > FCheckAssetsStyle::Create()
{
	TSharedRef< FSlateStyleSet > Style = MakeShareable(new FSlateStyleSet("CheckAssetsStyle"));
	Style->SetContentRoot(IPluginManager::Get().FindPlugin("CheckAssets")->GetBaseDir() / TEXT("Resources"));

	Style->Set("CheckAssets.PluginAction", new IMAGE_BRUSH_SVG(TEXT("Icon128_1"), Icon40x40));
	return Style;
}

void FCheckAssetsStyle::ReloadTextures()
{
	if (FSlateApplication::IsInitialized())
	{
		FSlateApplication::Get().GetRenderer()->ReloadTextureResources();
	}
}

const ISlateStyle& FCheckAssetsStyle::Get()
{
	return *StyleInstance;
}
