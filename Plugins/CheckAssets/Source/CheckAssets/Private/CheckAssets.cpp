// Copyright Epic Games, Inc. All Rights Reserved.

#include "CheckAssets.h"
#include "CheckAssetsStyle.h"
#include "CheckAssetsCommands.h"
#include "IStructureDetailsView.h"

#include "ToolMenus.h"
#include "FMatchRuleTypes.h"
#include "DetailCustomization/ScannerSettings.h"

// Engine Header
#include "ContentBrowserModule.h"
#include "IContentBrowserSingleton.h"
#include "ContentBrowserDataSource.h"
#include "AssetRegistry/IAssetRegistry.h"
#include "Toolkits/GlobalEditorCommonCommands.h"

#define LOCTEXT_NAMESPACE "FCheckAssetsModule"

int32 FCheckAssetsModule::TabNum = 0;

void FCheckAssetsModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
	FCheckAssetsStyle::Initialize();
	FCheckAssetsStyle::ReloadTextures();
	
	FCheckAssetsCommands::Register();
	
	PluginCommands = MakeShareable(new FUICommandList);

	PluginCommands->MapAction(
		FCheckAssetsCommands::Get().PluginAction,
		FExecuteAction::CreateRaw(this, &FCheckAssetsModule::PluginButtonClicked),
		FCanExecuteAction());
	
	PluginCommands->MapAction(FGlobalEditorCommonCommands::Get().FindInContentBrowser, FUIAction(
	FExecuteAction::CreateRaw(this, &FCheckAssetsModule::FindInContentBrowser),
	FCanExecuteAction::CreateRaw(this, &FCheckAssetsModule::IsAnythingSelected)
	));

	// Add nomad tabs
	FGlobalTabmanager::Get()->RegisterNomadTabSpawner(FTabName, FOnSpawnTab::CreateRaw(this, &FCheckAssetsModule::CreateToolTab))
		 .SetDisplayName(LOCTEXT("AssetAuditTitle", "Asset Audit2"))
		 .SetTooltipText(LOCTEXT("AssetAuditTooltip", "Open Asset Audit window, allows viewing detailed information about assets."))
		 .SetIcon(FSlateIcon(FAppStyle::GetAppStyleSetName(), "LevelEditor.Audit"));
	FGlobalTabmanager::Get()->RegisterDefaultTabWindowSize(FTabName, FVector2D(1080, 600));
	
	++FCheckAssetsModule::TabNum;
	UToolMenus::RegisterStartupCallback(FSimpleMulticastDelegate::FDelegate::CreateRaw(this, &FCheckAssetsModule::RegisterMenus));
}

void FCheckAssetsModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.

	UToolMenus::UnRegisterStartupCallback(this);

	UToolMenus::UnregisterOwner(this);

	FCheckAssetsStyle::Shutdown();
	FGlobalTabmanager::Get()->UnregisterNomadTabSpawner(FTabName);
	FCheckAssetsCommands::Unregister();
}

void FCheckAssetsModule::PluginButtonClicked()
{

	if (FCheckAssetsModule::TabNum < 1)
	{
		// Add nomad tabs
		FGlobalTabmanager::Get()->RegisterNomadTabSpawner(FTabName, FOnSpawnTab::CreateRaw(this, &FCheckAssetsModule::CreateToolTab))
			 .SetDisplayName(LOCTEXT("AssetAuditTitle", "Asset Audit2"))
			 .SetTooltipText(LOCTEXT("AssetAuditTooltip", "Open Asset Audit window, allows viewing detailed information about assets."))
			 .SetIcon(FSlateIcon(FAppStyle::GetAppStyleSetName(), "LevelEditor.Audit"));
		FGlobalTabmanager::Get()->RegisterDefaultTabWindowSize(FTabName, FVector2D(1080, 600));
		++FCheckAssetsModule::TabNum;
	}

    FGlobalTabmanager::Get()->TryInvokeTab(FTabName);
	FARFilter Filter;
	Filter.PackagePaths.Add("");
	SetFilterDelegate.Execute(Filter);
}


TSharedRef<SDockTab> FCheckAssetsModule::CreateToolTab(const FSpawnTabArgs& SpawnTabArgs)
{
	
	FContentBrowserModule& ContentBrowserModule = FModuleManager::Get().LoadModuleChecked<FContentBrowserModule>(TEXT("ContentBrowser"));

	// Configure filter for asset picker
	FAssetPickerConfig Config;
	Config.InitialAssetViewType = EAssetViewType::Column;
	Config.bAddFilterUI = true;
	Config.bShowPathInColumnView = true;
	Config.bSortByPathInColumnView = true;
	Config.bCanShowClasses = false;

	// Configure response to click and double-click
	Config.OnAssetDoubleClicked = FOnAssetDoubleClicked::CreateRaw(this, &FCheckAssetsModule::OnRequestOpenAsset);
	Config.OnGetAssetContextMenu = FOnGetAssetContextMenu::CreateRaw(this, &FCheckAssetsModule::OnGetAssetContextMenu);
	// Config.OnAssetTagWantsToBeDisplayed = FOnShouldDisplayAssetTag::CreateSP(this, &FCheckAssetsModule::CanShowColumnForAssetRegistryTag);
	// Config.OnGetCustomSourceAssets = FOnGetCustomSourceAssets::CreateSP(this, &FCheckAssetsModule::OnGetCustomSourceAssets);
	Config.SyncToAssetsDelegates.Add(&SyncToAssetsDelegate);
	// Config.OnShouldFilterAsset = FOnShouldFilterAsset::CreateSP(this, &FCheckAssetsModule::HandleFilterAsset);
	Config.GetCurrentSelectionDelegates.Add(&GetCurrentSelectionDelegate);
	Config.SetFilterDelegates.Add(&SetFilterDelegate);
	Config.bFocusSearchBoxWhenOpened = false;
	

	
	// Config.SaveSettingsName = TEXT("AssetManagementBrowser");
	
	// Hide path and type by default
	Config.HiddenColumnNames.Add(TEXT("Class"));
	Config.HiddenColumnNames.Add(TEXT("Path"));
	// Hide item disk size since we display a custom version here
	Config.HiddenColumnNames.Add(ContentBrowserItemAttributes::ItemDiskSize.ToString());

	// Add custom columns
	Config.CustomColumns.Emplace(RuleResult, LOCTEXT("RuleResult", "是否符合规范"), LOCTEXT("LODBiasNumberTooltip", "Texture Asset Type of this asset, if set"), UObject::FAssetRegistryTag::TT_Alphabetical, FOnGetCustomAssetColumnData::CreateRaw(this, &FCheckAssetsModule::GetStringValueForCustomColumn), FOnGetCustomAssetColumnDisplayText::CreateRaw(this, &FCheckAssetsModule::GetDisplayTextForCustomColumn));

	// Ignore these tags as we added them as custom columns
	AssetRegistryTagsToIgnore.Add(FPrimaryAssetId::PrimaryAssetTypeTag);
	AssetRegistryTagsToIgnore.Add(FPrimaryAssetId::PrimaryAssetNameTag);

	// Ignore blueprint tags
	AssetRegistryTagsToIgnore.Add(FBlueprintTags::ParentClassPath);
	AssetRegistryTagsToIgnore.Add(FBlueprintTags::BlueprintType);
	AssetRegistryTagsToIgnore.Add(FBlueprintTags::NumReplicatedProperties);
	AssetRegistryTagsToIgnore.Add(FBlueprintTags::NativeParentClassPath);
	AssetRegistryTagsToIgnore.Add(FBlueprintTags::IsDataOnly);
	AssetRegistryTagsToIgnore.Add(FBlueprintTags::NumNativeComponents);
	AssetRegistryTagsToIgnore.Add(FBlueprintTags::NumBlueprintComponents);
	
	ScannerConfig = MakeShareable(new FScannerConfig);
	CreateCustomDetails();
	return SAssignNew(CheckAssetTab, SDockTab)
		.TabRole(ETabRole::NomadTab)
		.ShouldAutosize(false)
		.OnTabClosed_Raw(this, &FCheckAssetsModule::OnClose)
		[
			SNew(SBorder)
			[
				SNew(SVerticalBox)
				+SVerticalBox::Slot()
				.AutoHeight()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					//SNew(SBorder)
					SNew(SButton)
					.VAlign(VAlign_Center)
					.HAlign(HAlign_Center)
					.Text(LOCTEXT("ScanAsset", "ScanAsset"))
					.ContentPadding(FMargin(0.0f, 0.0f, 2.0f, 0.0f))
					.OnClicked_Raw(this, &FCheckAssetsModule::ScanAsset)
				]

				+SVerticalBox::Slot()
				.AutoHeight()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					//SNew(SBorder)
					SNew(SButton)
					.VAlign(VAlign_Center)
					.HAlign(HAlign_Center)
					.Text(LOCTEXT("ClearAssets", "Clear Assets"))
					.ContentPadding(FMargin(0.0f, 0.0f, 2.0f, 0.0f))
					.OnClicked_Raw(this, &FCheckAssetsModule::ClearAssets)
					
				]
				
				+ SVerticalBox::Slot()
				.AutoHeight()
				.HAlign(HAlign_Fill)
				.VAlign(VAlign_Fill)
				[
					SNew(SHorizontalBox)
					+ SHorizontalBox::Slot()
					.VAlign(VAlign_Center)
					[
						SettingsView->GetWidget()->AsShared()
					]
				]
				+SVerticalBox::Slot()
				.FillHeight(1.f)
				[
					SNew(SBorder)
					.Padding(FMargin(3))
					.BorderImage(FAppStyle::GetBrush("ToolPanel.GroupBorder"))
					[
						ContentBrowserModule.Get().CreateAssetPicker(Config)
					]
				]
			]
		];	
}

void FCheckAssetsModule::OnClose(TSharedRef<SDockTab> Tab)
{
	--FCheckAssetsModule::TabNum;
}

FReply FCheckAssetsModule::ScanAsset() const
{

	TArray<FDirectoryPath> ScanDirectory = ScannerConfig->ScanFilters;
	FARFilter Filter;
	IAssetRegistry* AssetRegistry = IAssetRegistry::Get();
	for (auto Directory : ScanDirectory)
	{
		TArray<FAssetData> OutAssets;

		AssetRegistry->GetAssetsByPath(*Directory.Path, OutAssets);
		for (FAssetData Asset : OutAssets)
		{
			Filter.PackageNames.Add(Asset.PackageName);
		}
	}
	SetFilterDelegate.Execute(Filter);
	
	return FReply::Handled();
}

void FCheckAssetsModule::OnRequestOpenAsset(const FAssetData& AssetData) const
{
	TArray<FSoftObjectPath> AssetPaths;
	AssetPaths.Add(AssetData.GetSoftObjectPath());
	GEditor->GetEditorSubsystem<UAssetEditorSubsystem>()->OpenEditorsForAssets(AssetPaths);
}

void FCheckAssetsModule::FindInContentBrowser() const
{
	TArray<FAssetData> CurrentSelection = GetCurrentSelectionDelegate.Execute();
	if (CurrentSelection.Num() > 0)
	{
		const FContentBrowserModule& ContentBrowserModule = FModuleManager::Get().LoadModuleChecked<FContentBrowserModule>("ContentBrowser");
		ContentBrowserModule.Get().SyncBrowserToAssets(CurrentSelection);
	}
}

bool FCheckAssetsModule::IsAnythingSelected() const
{
	TArray<FAssetData> CurrentSelection = GetCurrentSelectionDelegate.Execute();
	return CurrentSelection.Num() > 0;
}

FString FCheckAssetsModule::GetStringValueForCustomColumn(FAssetData& AssetData, FName ColumnName) const
{
	FString OutValue = TEXT("True");
	if(ColumnName == RuleResult)
	{
		if(AssetData.AssetClassPath == UTexture2D::StaticClass()->GetClassPathName())
		{
			const UTexture2D* Texture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *AssetData.GetSoftObjectPath().ToString()));
			// 首先检查命名规范
			if(!AssetData.AssetName.ToString().StartsWith("T_"))
			{
				OutValue = TEXT("False");
			}
			
			
			// 是否启用LODBias检查
			if (ScannerConfig->bEnableLodBias)
			{
				const int32 LODBias = Texture->LODBias;
                if (LODBias > 2)
                {
                	OutValue = TEXT("False");
                }
			}
			// 压缩格式检查
			if(ScannerConfig->bEnableCompressionSetting)
			{
				TArray<FString> EndSuffix;
				EndSuffix.Add("_ORM");
				EndSuffix.Add("_ORH");
				EndSuffix.Add("_OGS");
				EndSuffix.Add("_Mask");
				EndSuffix.Add("_M");
				EndSuffix.Add("_NR");
				for (auto Suffix : EndSuffix)
				{
					if ( AssetData.AssetName.ToString().EndsWith(Suffix))
					{
						TEnumAsByte<enum TextureCompressionSettings> Compression = Texture->CompressionSettings;
						if (Compression.GetValue() != TextureCompressionSettings::TC_Masks)
						{
							OutValue = TEXT("False");
						}
					}
				}
			}
			if (ScannerConfig->bEnableDisplayedSize)
			{
				UE_LOG(LogTemp, Log, TEXT("size check"));
			}

		}
		if(AssetData.AssetClassPath == UStaticMesh::StaticClass()->GetClassPathName())
		{
			const UStaticMesh* StaticMesh = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), nullptr, *AssetData.GetSoftObjectPath().ToString()));
			// 首先检查命名规范
			if(!AssetData.AssetName.ToString().StartsWith("SM_"))
			{
				OutValue = TEXT("False");
			}
			
			if (ScannerConfig->bEnableLodNumber)
			{
				int32 LODNum = StaticMesh->GetNumLODs();
				if (LODNum > 4)
				{
					OutValue = TEXT("False");
				}
			}

		}

	}
	return OutValue;
}

FText FCheckAssetsModule::GetDisplayTextForCustomColumn(FAssetData& AssetData, FName ColumnName) const
{
	FText OutValue = LOCTEXT("Value", "True");
	if(ColumnName == RuleResult)
	{
		if(AssetData.AssetClassPath == UTexture2D::StaticClass()->GetClassPathName())
		{
			const UTexture2D* Texture = Cast<UTexture2D>(StaticLoadObject(UTexture2D::StaticClass(), nullptr, *AssetData.GetSoftObjectPath().ToString()));
			// 首先检查命名规范
			if(!AssetData.AssetName.ToString().StartsWith("T_"))
			{
				OutValue = LOCTEXT("Value", "False");
			}
			// 是否启用LODBias检查
			if (ScannerConfig->bEnableLodBias)
			{
				const int32 LODBias = Texture->LODBias;
				if (LODBias > 2)
				{
					OutValue = LOCTEXT("Value", "False");
				}
			}
			// 压缩格式检查
			if(ScannerConfig->bEnableCompressionSetting)
			{
				TArray<FString> EndSuffix;
				EndSuffix.Add("_ORM");
				EndSuffix.Add("_ORH");
				EndSuffix.Add("_OGS");
				EndSuffix.Add("_Mask");
				EndSuffix.Add("_M");
				EndSuffix.Add("_NR");
				for (auto Suffix : EndSuffix)
				{
					if ( AssetData.AssetName.ToString().EndsWith(Suffix))
					{
						TEnumAsByte<enum TextureCompressionSettings> Compression = Texture->CompressionSettings;
						if (Compression.GetValue() != TextureCompressionSettings::TC_Masks)
						{
							OutValue = LOCTEXT("Value", "False");
						}
					}
				}
			}
			if (ScannerConfig->bEnableDisplayedSize)
			{
				UE_LOG(LogTemp, Log, TEXT("size check"));
			}

		}
		if(AssetData.AssetClassPath == UStaticMesh::StaticClass()->GetClassPathName())
		{
			const UStaticMesh* StaticMesh = Cast<UStaticMesh>(StaticLoadObject(UStaticMesh::StaticClass(), nullptr, *AssetData.GetSoftObjectPath().ToString()));
			// 首先检查命名规范
			if(!AssetData.AssetName.ToString().StartsWith("SM_"))
			{
				OutValue = LOCTEXT("Value", "False");
			}
			
			if (ScannerConfig->bEnableLodNumber)
			{
				int32 LODNum = StaticMesh->GetNumLODs();
				if (LODNum > 4)
				{
					OutValue = LOCTEXT("Value", "False");
				}
			}

		}

	}
	return OutValue;
}
TSharedPtr<SWidget> FCheckAssetsModule::OnGetAssetContextMenu(const TArray<FAssetData>& SelectedAssets) const
{
	FMenuBuilder MenuBuilder(/*bInShouldCloseWindowAfterMenuSelection=*/ true, Commands);
	MenuBuilder.BeginSection(TEXT("Asset"), NSLOCTEXT("ReferenceViewerSchema", "AssetSectionLabel", "Asset"));
	{
		MenuBuilder.AddMenuEntry(FGlobalEditorCommonCommands::Get().FindInContentBrowser);
	}
	MenuBuilder.EndSection();

	return MenuBuilder.MakeWidget();

}

void FCheckAssetsModule::AddAssetsToList(const TArray<FName>& PackageNamesToView, bool bReplaceExisting)
{
	TArray<FName> AssetNames;
	UE_LOG(LogTemp, Warning, TEXT("This is AddAssetsToList"));
	

}

FReply FCheckAssetsModule::ClearAssets()
{

	TArray<FDirectoryPath> ScanDirectory = ScannerConfig->ScanFilters;
	FARFilter Filter;
	Filter.PackagePaths.Add("");
	SetFilterDelegate.Execute(Filter);
	AddAssetsToList(TArray<FName>(), true);
	UE_LOG(LogTemp, Warning, TEXT("This is ClearAssets"));
	return FReply::Handled();
}

void FCheckAssetsModule::OnGetCustomSourceAssets(const FARFilter& Filter, TArray<FAssetData>& OutAssets) const
{
	UE_LOG(LogTemp, Warning, TEXT("This is CustomizeDetails function, path is %s"));
}

void FCheckAssetsModule::CreateCustomDetails()
{
	FPropertyEditorModule& EditModule = FModuleManager::Get().GetModuleChecked<FPropertyEditorModule>("PropertyEditor");

	FDetailsViewArgs DetailsViewArgs;
	{
		// 细节面板是否能搜索
		DetailsViewArgs.bAllowSearch = true;
		// 是否隐藏选择提示
		DetailsViewArgs.bHideSelectionTip = true;
		DetailsViewArgs.bLockable = false;
		DetailsViewArgs.bSearchInitialKeyFocus = true;
		DetailsViewArgs.bUpdatesFromSelection = false;
		DetailsViewArgs.NotifyHook = nullptr;
		DetailsViewArgs.bShowOptions = true;
		DetailsViewArgs.bShowModifiedPropertiesOption = false;
		DetailsViewArgs.bShowScrollBar = false;
		DetailsViewArgs.bShowOptions = true;
		DetailsViewArgs.bUpdatesFromSelection= true;
	}

	FStructureDetailsViewArgs StructureViewArgs;
	{
		StructureViewArgs.bShowObjects = true;
		StructureViewArgs.bShowAssets = true;
		StructureViewArgs.bShowClasses = true;
		StructureViewArgs.bShowInterfaces = true;
	}

	SettingsView = EditModule.CreateStructureDetailView(DetailsViewArgs, StructureViewArgs, nullptr);
	FStructOnScope* Struct = new FStructOnScope(FScannerConfig::StaticStruct(), (uint8*)ScannerConfig.Get());
	// 这里是指定一个当前 SettingsView 的 DetailCustomization 类实例，用于在创建 SettingView 控件的时候进行定制化的操作
	SettingsView->GetDetailsView()->RegisterInstancedCustomPropertyLayout(FScannerConfig::StaticStruct(),FOnGetDetailCustomizationInstance::CreateStatic(&FScannerSettingsDetails::MakeInstance));
	SettingsView->SetStructureData(MakeShareable(Struct));
	
	
}

void FCheckAssetsModule::RegisterMenus()
{
	// Owner will be used for cleanup in call to UToolMenus::UnregisterOwner
	FToolMenuOwnerScoped OwnerScoped(this);

	{
		UToolMenu* Menu = UToolMenus::Get()->ExtendMenu("LevelEditor.MainMenu.Window");
		{
			FToolMenuSection& Section = Menu->FindOrAddSection("WindowLayout");
			Section.AddMenuEntryWithCommandList(FCheckAssetsCommands::Get().PluginAction, PluginCommands);
		}
	}

	{
		UToolMenu* ToolbarMenu = UToolMenus::Get()->ExtendMenu("LevelEditor.LevelEditorToolBar.PlayToolBar");
		{
			// FToolMenuSection& Section = ToolbarMenu->FindOrAddSection("PluginTools");
			FToolMenuSection& Section = ToolbarMenu->AddSection("CheckAsset", LOCTEXT("CheckAsset","CheckAsset"));
			{
				FToolMenuEntry& Entry = Section.AddEntry(FToolMenuEntry::InitToolBarButton(FCheckAssetsCommands::Get().PluginAction));
				Entry.SetCommandList(PluginCommands);
			}
		}
	}

	
}


#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FCheckAssetsModule, CheckAssets)