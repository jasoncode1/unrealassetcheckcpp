﻿#include "ScannerSettings.h"
#include "FMatchRuleTypes.h"
#include "DetailLayoutBuilder.h"
#include "DetailCategoryBuilder.h"
#include "DetailWidgetRow.h"

#define LOCTEXT_NAMESPACE "ScannerSettingsDetails"

TSharedRef<IDetailCustomization> FScannerSettingsDetails::MakeInstance()
{
	return MakeShareable(new FScannerSettingsDetails());
}


//  自定义细节面板扩展示例 暂不删除 
void FScannerSettingsDetails::CustomizeDetails(IDetailLayoutBuilder& DetailBuilder)
{
	TArray<TSharedPtr<FStructOnScope>> StructBeingCustomized;
	DetailBuilder.GetStructsBeingCustomized(StructBeingCustomized);

	FScannerConfig* ScannerSettingIns = (FScannerConfig*)StructBeingCustomized[0].Get()->GetStructMemory();
	
	
	IDetailCategoryBuilder& DetailCategory = DetailBuilder.EditCategory("Global");
	// 不显示Advanced 选项
	// DetailCategory.SetShowAdvanced(true);
	
	DetailCategory.AddCustomRow(LOCTEXT("CustomRow1", "CustomRow2"),true)
	.ValueContent()
	[
		SNew(SButton)
		.IsEnabled(false)
		.Text(LOCTEXT("Check", "Check"))
		.OnClicked_Lambda([this, ScannerSettingIns]()
		{
			if (ScannerSettingIns)
			{
				ScannerSettingIns->ScanDirectoryPath();
			}
			return(FReply::Handled());
		})
	];
	
	
}


#undef LOCTEXT_NAMESPACE
