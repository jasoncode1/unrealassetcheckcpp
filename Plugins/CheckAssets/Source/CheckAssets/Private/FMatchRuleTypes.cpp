﻿#include "FMatchRuleTypes.h"

#include "AssetRegistry/AssetRegistryModule.h"



TArray<FDirectoryPath> FScannerConfig::GetScanDirectory() const
{
	TArray<FDirectoryPath> Result;
	if(ScanFilters.Num()>0)
	{
		Result.Append(ScanFilters);
	}
	
	return Result;
	
}

void FScannerConfig::ScanDirectoryPath()
{
	// TArray<FDirectoryPath> Result = GetScanDirectory();

	if (bEnableLodBias)
	{
		UE_LOG(LogTemp, Warning, TEXT("bEnableLodBias is true"));
	}
	for (FDirectoryPath DirectoryPath : ScanFilters)
	{
		TArray<FAssetData> AssetData;
		bool bRecursive = true;
		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
		AssetRegistryModule.Get().GetAssetsByPath(FName(*DirectoryPath.Path), AssetData, bRecursive);
		for (FAssetData Data : AssetData)
		{
			
			UE_LOG(LogTemp, Warning, TEXT("This is CustomizeDetails function, path is %s"), *Data.AssetName.ToString());
		}
		
	}
	
	// if(Result.Contains("Texture"))
	// {
	// 	TArray<FDirectoryPath> *TResult = Result.Find("Texture");
	// 	for (FDirectoryPath DirectoryPath : *TResult)
	// 	{
	//
	// 		TArray<FAssetData> AssetData;
	// 		bool bRecursive = true;
	// 		FAssetRegistryModule& AssetRegistryModule = FModuleManager::LoadModuleChecked<FAssetRegistryModule>("AssetRegistry");
	// 		AssetRegistryModule.Get().GetAssetsByPath(FName(*DirectoryPath.Path), AssetData, bRecursive);
	// 		
	// 		for (FAssetData Data : AssetData)
	// 		{
	// 			
	// 			UE_LOG(LogTemp, Warning, TEXT("This is CustomizeDetails function, path is %s"), *Data.AssetName.ToString());
	// 		}
	// 		
	// 		
	// 	}
	// }
	// if(Result.Contains("StaticMesh"))
	// {
	// 	TArray<FDirectoryPath> *SMResult = Result.Find("StaticMesh");
	// 	for (FDirectoryPath DirectoryPath : *SMResult)
	// 	{
	// 		
	// 		UE_LOG(LogTemp, Warning, TEXT("This is CustomizeDetails function, path is %s"), *DirectoryPath.Path);
	// 	}
	// }

}
