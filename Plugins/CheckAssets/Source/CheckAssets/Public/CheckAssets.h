// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once


#include "CoreMinimal.h"
#include "FMatchRuleTypes.h"
#include "ContentBrowserDelegates.h"


class FCheckAssetsModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
	
	/** This function will be bound to Command. */
	void PluginButtonClicked();
	

private:

	const FName FTabName = FName("CheckAsset");
	
	// 可以用MapAction映射 关联触发函数 类似于信号槽
	TSharedPtr<class FUICommandList> PluginCommands;

	// 拿到全局的CommandList 做函数映射
	TSharedPtr<FUICommandList> Commands;
	
	// 创建一个Tab窗口
	TSharedPtr<SDockTab> CheckAssetTab;
	
	static inline const FName RuleResult = FName("RuleResult");

	static int32 TabNum;
private:
	
	// 创建一个面板 继承子 必须实现
	TSharedRef<SDockTab> CreateToolTab(const FSpawnTabArgs& SpawnTabArgs);
	
	// 注册菜单
	void RegisterMenus();
	
	// 实例化一个 IStructureDetailsView
	TSharedPtr<IStructureDetailsView> SettingsView;
	
	// 用结构体进行属性映射
	TSharedPtr<FScannerConfig> ScannerConfig;
	
	// 同步委托
	FSyncToAssetsDelegate SyncToAssetsDelegate;
	
	// 过滤委托
	FSetARFilterDelegate SetFilterDelegate;
	
	// 忽略资产Tag
	TSet<FName> AssetRegistryTagsToIgnore;
	// 拿到全局的CommandList 做函数映射
	
	FGetCurrentSelectionDelegate GetCurrentSelectionDelegate;
	
	// 创建自定义细节面板
	void CreateCustomDetails();
	
	/** This function will be check assets data */
	FReply ScanAsset() const;
	
	// 清空资产
	FReply ClearAssets();


	void OnClose(TSharedRef<SDockTab> Tab);
	
	// 在ContentBrowser中显示资产
	void FindInContentBrowser() const;
	
	// 选中资产
	bool IsAnythingSelected() const;
	
	// 展开资产的属性编辑界面
	void OnRequestOpenAsset(const FAssetData& AssetData) const;
	
	// 添加资产到列表 TODO
	void AddAssetsToList(const TArray<FName>& PackageNamesToView, bool bReplaceExisting);
	
	// TODO
	void OnGetCustomSourceAssets(const FARFilter& Filter, TArray<FAssetData>& OutAssets) const;
	
	// 创建选中资产的右键菜单
	TSharedPtr<SWidget> OnGetAssetContextMenu(const TArray<FAssetData>& SelectedAssets) const;
	
	// 获取Column相应的属性转成FString
	FString GetStringValueForCustomColumn(FAssetData& AssetData, FName ColumnName) const;
	
	// 获取Column相应的属性转成FText
	FText GetDisplayTextForCustomColumn(FAssetData& AssetData, FName ColumnName) const;

};



