﻿#pragma once

// engine header
#include "AssetData.h"
#include "CoreMinimal.h"
#include "Engine/EngineTypes.h"
#include "Engine/DataTable.h"
#include "FMatchRuleTypes.generated.h"

USTRUCT(BlueprintType)
struct CHECKASSETS_API FScannerConfig
{
	GENERATED_USTRUCT_BODY()
public:
	FORCEINLINE static FScannerConfig* Get()
	{
		static FScannerConfig StaticIns;
		return &StaticIns;
	}
	// 资源类型
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="扫描资源类型",Category = "Filter")
	// UClass* ScanAssetType = nullptr;

	
	// 扫描Texture资源路径
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="扫描资源路径",Category = "Global",meta = (RelativeToGameContentDir, LongPackageName))
	TArray<FDirectoryPath> ScanFilters;

	// 是否启用当前规则
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="启用LOD Bias检查",Category = "Texture")
	bool bEnableLodBias = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="启用Compression Setting检查",Category = "Texture")
	bool bEnableCompressionSetting = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="启用Displayed Size检查",Category = "Texture")
	bool bEnableDisplayedSize = true;

	// 扫描Static Mesh资源路径
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="扫描资源路径",Category = "Static Mesh",meta = (RelativeToGameContentDir, LongPackageName))
	// TArray<FDirectoryPath> SMScanFilters;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="启用LOD Number检查",Category = "Static Mesh")
	bool bEnableLodNumber = true;

	TArray<FDirectoryPath> GetScanDirectory()const;
	void ScanDirectoryPath();
	
	
};

USTRUCT(BlueprintType)
struct CHECKASSETS_API FScannerMatchRule:public FTableRowBase
{
	GENERATED_USTRUCT_BODY()
public:
	// 规则名
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="规则名",Category = "Filter")
	FString RuleName;
	// 规则描述
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="规则描述",Category = "Filter")
	FString RuleDescribe;
	// 是否启用当前规则
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="启用当前规则",Category = "Filter")
	bool bEnableRule = true;

	// 扫描资源路径
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="扫描资源路径",Category = "Filter",meta = (RelativeToGameContentDir, LongPackageName))
	TArray<FDirectoryPath> ScanFilters;
	// 资源类型
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="扫描资源类型",Category = "Filter")
	UClass* ScanAssetType = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="递归子类类型",Category = "Filter")
	bool RecursiveClasses = true;
	// // 命名匹配规则
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="命名匹配",Category = "Filter")
	// FNameMatchRule NameMatchRules;
	// // 路径匹配规则
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="路径匹配",Category = "Filter")
	// FPathMatchRule PathMatchRules;
	// // 属性匹配规则
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="属性匹配",Category = "Filter")
	// FPropertyMatchRule PropertyMatchRules;
	// // 自定义匹配规则（派生自UOperatorBase的类）
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="自定义匹配规则",Category = "Filter")
	// TArray<TSubclassOf<UOperatorBase>> CustomRules;
	// // 忽略本规则的路径、资源列表
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="扫描时忽略的资源",Category = "Filter")
	// FAssetFilters IgnoreFilters;
	// // 是否开启匹配资源的后处理
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="启用资源扫描后处理",Category = "Filter")
	// bool bEnablePostProcessor = false;
	// // 当扫描完毕之后，对命中规则的资源进行处理
	// UPROPERTY(EditAnywhere, BlueprintReadWrite,DisplayName="后处理规则",Category = "Filter",meta=(EditCondition="bEnablePostProcessor"))
	// TArray<TSubclassOf<UScannnerPostProcessorBase>> PostProcessors;
	//
	// bool operator==(const FScannerMatchRule& R)const;
	//
	// bool HasValidRules()const { return (NameMatchRules.Rules.Num() || PathMatchRules.Rules.Num() || PropertyMatchRules.MatchRules.Num() || CustomRules.Num()); }
};