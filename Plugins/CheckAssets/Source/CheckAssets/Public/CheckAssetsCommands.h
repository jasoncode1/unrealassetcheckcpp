// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Framework/Commands/Commands.h"
#include "CheckAssetsStyle.h"


class FCheckAssetsCommands : public TCommands<FCheckAssetsCommands>
{
public:

	FCheckAssetsCommands()
		: TCommands<FCheckAssetsCommands>(TEXT("CheckAssets"), NSLOCTEXT("Contexts", "CheckAssets", "CheckAssets Plugin"), NAME_None, FCheckAssetsStyle::GetStyleSetName())
	{
	}

	// TCommands<> interface
	virtual void RegisterCommands() override;

public:
	TSharedPtr< FUICommandInfo > PluginAction;
};
